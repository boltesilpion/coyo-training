(function (angular) {
  'use strict';

  angular
      .module('coyo.custom.apps.example')
      .controller('ExampleAppController', ExampleAppController);

  /**
   * Controller for the example app.
   *
   * @requires app
   * @constructor
   */
  function ExampleAppController(app, ExampleModel, example, exampleModalService) {
    var vm = this;
    vm.app = app;
    vm.message = example ? example.message : '';
    vm.busy = false;

    vm.changeMessage = changeMessage;
    
    function changeMessage() {
      exampleModalService.enter(vm.message).then(function (message) {
        ExampleModel.save(vm.app, message).then(function () {
          vm.message = message;
        });
      });
    }
  }

})(angular);
