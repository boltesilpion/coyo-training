(function (angular) {
  'use strict';

  angular
      .module('coyo.custom.apps.example')
      .controller('ExampleSettingsController', ExampleSettingsController);

  function ExampleSettingsController($log, $scope) {
    var vm = this;
    $log.debug('[ExampleSettingsController] vm, $scope', vm, $scope);
  }

})(angular);
