(function (angular) {
  'use strict';

  angular
      .module('coyo.custom.apps.example', [
        'coyo.apps.api'
      ])
      .config(registerApp)
      .config(registerTranslations);

  function registerApp(appRegistryProvider) {
    appRegistryProvider.register({
      name: 'APP.EXAMPLE.NAME',
      description: 'APP.EXAMPLE.DESCRIPTION',
      key: 'example',
      icon: 'zmdi-shape',
      states: [
        {
          templateUrl: 'app/apps/example/example.html',
          controller: 'ExampleAppController',
          controllerAs: 'ctrl',
          resolve: {
            example: /*@ngInject*/ function (ExampleModel, app) {
              var context = {
                senderId: app.senderId,
                appId: app.id
              };
              return ExampleModel.get(context, {});
            }
          }
        }
      ],
      settings: {
        templateUrl: 'app/apps/example/example-settings.html',
        controller: 'ExampleSettingsController as exampleCtrl'
      }
    });
  }

  function registerTranslations($translateProvider) {
    /* eslint-disable quotes */
    $translateProvider.translations('en', {
      "OK": "OK",
      "APP.EXAMPLE.DESCRIPTION": "An app to demonstrate the customization of Coyo.",
      "APP.EXAMPLE.MESSAGE.CHANGE": "Change message",
      "APP.EXAMPLE.MESSAGE.MESSAGE": "Message",
      "APP.EXAMPLE.MODAL.MESSAGE.TITLE": "New Message",
      "APP.EXAMPLE.MODAL.MESSAGE.TEXT": "Enter a new message",
      "APP.EXAMPLE.NAME": "Example App",
      "APP.EXAMPLE.SETTINGS.DISPLAYNAME.HELP": "The display name inside of the app",
      "APP.EXAMPLE.SETTINGS.DISPLAYNAME.STRING": "Display name"
    });
    /* eslint-enable quotes */
  }

})(angular);
