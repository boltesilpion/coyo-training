(function (angular) {
  'use strict';

  angular
      .module('coyo.custom.widgets.example', [
        'coyo.widgets.api'
      ])
      .config(registerExampleWidget)
      .config(registerTranslations);

  function registerExampleWidget(widgetRegistryProvider) {
    widgetRegistryProvider.register({
      key: 'example',
      name: 'WIDGET.EXAMPLE.NAME',
      description: 'WIDGET.EXAMPLE.DESCRIPTION',
      icon: 'zmdi-code',
      directive: 'example-widget',
      settings: {
        controller: 'ExampleWidgetSettingsController',
        templateUrl: 'app/widgets/example/example-widget-settings.html'
      }
    });
  }

  function registerTranslations($translateProvider) {
    /* eslint-disable quotes */
    $translateProvider.translations('en', {
      "WIDGET.EXAMPLE.DESCRIPTION": "A widget to demonstrate the customization of Coyo.",
      "WIDGET.EXAMPLE.NAME": "Example Widget",
      "WIDGET.EXAMPLE.SETTINGS.TITLE.HELP": "Enter the title of the widget",
      "WIDGET.EXAMPLE.SETTINGS.TITLE.STRING": "Title",
      "WIDGET.EXAMPLE.SETTINGS.SOMESTRING.HELP": "Enter some string that will be displayed",
      "WIDGET.EXAMPLE.SETTINGS.SOMESTRING.STRING": "String"
    });
    /* eslint-enable quotes */
  }

})(angular);
