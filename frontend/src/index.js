angular.element(document).ready(function () {
  /**
   * Why manually bootstrap AngularJS with Cordova/PhoneGap/Ionic?
   *
   * The issue is that you could have AngularJS code that relies on Cordova/PhoneGap/Ionic plugins,
   * and those plugins won't be ready until after AngularJS has started because Cordova takes longer
   * to get up and running on a device than the plain old Javascript code for AngularJS does.
   *
   * Source: http://stackoverflow.com/questions/21556090/cordova-angularjs-device-ready/27450072#27450072
   */
  if (window.cordova) {
    console.log("Running in Cordova, starting the application once 'deviceready' event fires.");
    document.addEventListener('deviceready', function () {
      console.log("'deviceready' event has fired, starting Coyo now.");
      angular.bootstrap(document, ['coyo.custom']);
    }, false);
  } else {
    console.log("Running in browser, starting custom Coyo now.");
    angular.bootstrap(document, ['coyo.custom']);
  }
});
