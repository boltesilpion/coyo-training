package com.mindsmash.coyo.custom.apps.example;

import com.mindsmash.coyo.web.dto.services.BaseDtoService;
import org.springframework.stereotype.Service;

/**
 * Example app DTO service.
 * Transforms an Example to an ExampleResponseBody.
 */
@Service
public class ExampleDtoService extends BaseDtoService<Example, ExampleResponseBody> {

    @Override
    public ExampleResponseBody transform(Example model) {
        return new ExampleResponseBody(model);
    }
}
