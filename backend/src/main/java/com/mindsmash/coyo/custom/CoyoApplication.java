package com.mindsmash.coyo.custom;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.mindsmash.coyo.configuration.EnableCoyo;

@SpringBootApplication
@EnableCoyo
public class CoyoApplication {

    public static void main(String[] args) {
        SpringApplication.run(CoyoApplication.class, args);
    }
}
